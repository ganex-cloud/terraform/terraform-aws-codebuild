variable "name" {
  type        = "string"
  description = "The projects name."
}

variable "s3_bucket_arn" {
  type        = "string"
  description = "The S3 Bucket ARN"
}

variable "s3_bucket_path" {
  default     = "/source.zip"
  type        = "string"
  description = "The S3 object Key or folder"
}

variable "environment_type" {
  default     = "LINUX_CONTAINER"
  type        = "string"
  description = "The type of build environment to use for related builds."
}

variable "compute_type" {
  default     = "BUILD_GENERAL1_SMALL"
  type        = "string"
  description = "Information about the compute resources the build project will use."
}

variable "image" {
  default     = "aws/codebuild/docker:18.09.0"
  type        = "string"
  description = "The image identifier of the Docker image to use for this build project."
}

variable "privileged_mode" {
  default     = true
  type        = "string"
  description = "If set to true, enables running the Docker daemon inside a Docker container."
}

variable "encryption_key" {
  default     = ""
  type        = "string"
  description = "The KMS CMK to be used for encrypting the build project's build output artifacts."
}

variable "build_timeout" {
  default     = 15
  type        = "string"
  description = "How long in minutes to wait until timing out any related build that does not get marked as completed."
}

variable "iam_path" {
  default     = "/"
  type        = "string"
  description = "Path in which to create the IAM Role and the IAM Policy."
}

variable "iam_name" {
  type        = "string"
  description = "Name of IAM Role and the IAM Policy."
}

variable "description" {
  default     = "Managed by Terraform"
  type        = "string"
  description = "The description of the all resources."
}

variable "tags" {
  default     = {}
  type        = "map"
  description = "A mapping of tags to assign to all resources."
}

variable "role_arn" {
  description = "Role ARN that are trusted to use codebuild."
  type        = "list"
  default     = []
}

variable "environment_variables" {
  type = "list"

  default = [{
    "name"  = "NO_ADDITIONAL_BUILD_VARS"
    "value" = "TRUE"
  }]

  description = "A list of maps, that contain both the key 'name' and the key 'value' to be used as additional environment variables for the build."
}

variable "aws_region" {
  type        = "string"
  default     = ""
  description = "(Optional) AWS Region, e.g. us-east-1. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
}

variable "aws_account_id" {
  type        = "string"
  default     = ""
  description = "(Optional) AWS Account ID. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
}

variable "custom_policy" {
  type        = "string"
  default     = ""
  description = "The custom extra policy document. This is a JSON formatted string."
}

variable "vpc_id" {
  description = "ID of VPC to run within"
  default     = ""
}

variable "subnets" {
  description = "IDs of subnets to run within"
  default     = []
}

variable "security_groups" {
  description = "IDs of security groups to attach"
  default     = []
}

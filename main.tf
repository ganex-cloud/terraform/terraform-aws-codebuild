data "aws_caller_identity" "default" {}
data "aws_region" "default" {}

resource "aws_codebuild_project" "default" {
  count        = "${var.vpc_id == "" ? 1 : 0}"
  name         = "${var.name}"
  description  = "${var.description}"
  service_role = "${aws_iam_role.default.arn}"

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    type            = "${var.environment_type}"
    compute_type    = "${var.compute_type}"
    image           = "${var.image}"
    privileged_mode = "${var.privileged_mode}"

    environment_variable {
      "name"  = "AWS_DEFAULT_REGION"
      "value" = "${signum(length(var.aws_region)) == 1 ? var.aws_region : data.aws_region.default.name}"
    }

    environment_variable {
      "name"  = "AWS_ACCOUNT_ID"
      "value" = "${signum(length(var.aws_account_id)) == 1 ? var.aws_account_id : data.aws_caller_identity.default.account_id}"
    }
  }

  source {
    type     = "S3"
    location = "${var.s3_bucket_arn}${var.s3_bucket_path}"
  }

  encryption_key = "${var.encryption_key}"
  build_timeout  = "${var.build_timeout}"
  tags           = "${merge(map("Name", var.name), var.tags)}"
}

resource "aws_codebuild_project" "default_vpc" {
  count        = "${length(var.vpc_id) > 0 ? 1 : 0}"
  name         = "${var.name}"
  description  = "${var.description}"
  service_role = "${aws_iam_role.default.arn}"

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    type            = "${var.environment_type}"
    compute_type    = "${var.compute_type}"
    image           = "${var.image}"
    privileged_mode = "${var.privileged_mode}"

    environment_variable {
      "name"  = "AWS_DEFAULT_REGION"
      "value" = "${signum(length(var.aws_region)) == 1 ? var.aws_region : data.aws_region.default.name}"
    }

    environment_variable {
      "name"  = "AWS_ACCOUNT_ID"
      "value" = "${signum(length(var.aws_account_id)) == 1 ? var.aws_account_id : data.aws_caller_identity.default.account_id}"
    }
  }

  source {
    type     = "S3"
    location = "${var.s3_bucket_arn}${var.s3_bucket_path}"
  }

  vpc_config {
    security_group_ids = ["${var.security_groups}"]
    subnets            = ["${var.subnets}"]
    vpc_id             = "${var.vpc_id}"
  }

  encryption_key = "${var.encryption_key}"
  build_timeout  = "${var.build_timeout}"
  tags           = "${merge(map("Name", var.name), var.tags)}"
}

resource "aws_iam_role" "default" {
  name               = "${var.iam_name}"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
  path               = "${var.iam_path}"
  description        = "${var.description}"
  tags               = "${merge(map("Name", var.iam_name), var.tags)}"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }

    principals {
      type        = "AWS"
      identifiers = "${var.role_arn}"
    }
  }
}

resource "aws_iam_policy" "default" {
  name        = "${var.iam_name}"
  policy      = "${data.aws_iam_policy_document.policy.json}"
  path        = "${var.iam_path}"
  description = "${var.description}"
}

data "aws_iam_policy_document" "policy" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetBucketLocation",
    ]

    resources = ["arn:aws:s3:::*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketAcl",
    ]

    resources = [
      "${var.s3_bucket_arn}",
      "${var.s3_bucket_arn}${var.s3_bucket_path}",
      "${var.s3_bucket_arn}${var.s3_bucket_path}/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "ecr:GetAuthorizationToken",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeDhcpOptions",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeVpcs",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2:CreateNetworkInterfacePermission",
    ]

    resources = ["arn:aws:ec2:${data.aws_region.default.name}:${data.aws_caller_identity.default.account_id}:network-interface/*"]

    condition {
      test     = "StringEquals"
      variable = "ec2:AuthorizedService"
      values   = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "default" {
  role       = "${aws_iam_role.default.name}"
  policy_arn = "${aws_iam_policy.default.arn}"
}

resource "aws_iam_policy" "custom" {
  count  = "${length(var.custom_policy) > 0 ? 1 : 0}"
  name   = "${var.iam_name}-custom"
  policy = "${var.custom_policy}"
}

resource "aws_iam_role_policy_attachment" "custom" {
  count      = "${length(var.custom_policy) > 0 ? 1 : 0}"
  role       = "${aws_iam_role.default.name}"
  policy_arn = "${aws_iam_policy.custom.arn}"
}

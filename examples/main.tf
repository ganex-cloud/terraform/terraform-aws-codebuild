module "codebuild-NAME" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-codebuild.git?ref=master"
  name           = "NAME"
  s3_bucket_arn  = "${module.s3-BUCKET.bucket_arn}"
  compute_type   = "BUILD_GENERAL1_SMALL"
  image          = "aws/codebuild/docker:18.09.0"
  build_timeout  = 10
  iam_name       = "ROLENAME"
  s3_bucket_path = "/*"
}
